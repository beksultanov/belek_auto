/* 
Get the width of a single item, multiply by total # of items (10 in this case), then divide by 2 as well.
*/

const item = document.querySelector(".item");
const items = document.querySelectorAll(".item");
const marquee = document.querySelector(".marquee");

const total = Array.from(items).length;

marquee.style.width = `${(item.clientWidth * total) / 8}px`;