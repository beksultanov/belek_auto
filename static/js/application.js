  $(".application_create_form").on("submit", function (e) {
    e.preventDefault()
    var form = $(this);
    $.ajax({
        url: '/create_application/',
        data: form.serialize(),
        type: 'post',
        dataType: 'json',
        success: function (data) {
            if (data.form_is_valid) {
            $(".application_create_form").html(data.html_form)
              toastr.success('Ваша заявка успешно принята!',
                  "Новая заявка",
                  {
                    "closeButton": true,
                    "progressBar": false,
                    "positionClass": "toast-top-center",
                  },
                  )// <-- This is just a placeholder for now for testing
            } else {
                toastr.warning('Пожалуйста, проверьте введенные вами данные',
                  "Ошибка",
                  {
                    "closeButton": true,
                    "progressBar": false,
                    "positionClass": "toast-top-center",
                  },
                  )
            }
        },
    });
    return false;
}
)

$("#subscribe").on("submit", function (e) {
e.preventDefault()
var form = $(this);
$.ajax({
    url: '',
    data: form.serialize(),
    type: 'post',
    dataType: 'json',
    success: function (data) {
        if (data.status) {
          toastr.success('Вы успешно подписались на рассылку',
              "Подписка на рассылку",
              {
                "closeButton": true,
                "progressBar": false,
                "positionClass": "toast-top-center",
              },
              )// <-- This is just a placeholder for now for testing
        } else {
            toastr.warning('Пожалуйста, проверьте введенные вами данные',
              "Ошибка",
              {
                "closeButton": true,
                "progressBar": false,
                "positionClass": "toast-top-center",
              },
              )
        }
    },
});
return false;
}
)