function initMapListing() {
  var myLatLng = {lat: 47.6205588, lng: -122.3212725};
  var map = new google.maps.Map(document.getElementById('listing-map'), {
    zoom: 15,
    center: myLatLng,
    scrollwheel: false
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Belek Auto'
  });
  var contentString = '<div id="content"><h1>Golden Gate Bridge</h1><p>The Golden Gate Bridge is a' +
  'suspension bridge spanning the Golden Gate, the one-mile-wide (1.6 km) strait connecting San' +
  'Francisco Bay and the Pacific Ocean. The structure links the American city of San Francisco,' +
  'California - the northern tip of the San Francisco Peninsula - to Marin County, carrying' +
  'both U.S. Route 101 and California State Route 1 across the strait. The bridge is one of' +
  'the most internationally recognized symbols of San Francisco, California, and the United States.' +
  'It has been declared one of the Wonders of the Modern World by the American Society of Civil' +
  'Engineers.</p><p>Attribution: Golden Gate Bridge <a href="https://en.wikipedia.org/wiki/Golden_Gate_Bridge">' +
  'https://en.wikipedia.org/wiki/Golden_Gate_Bridge</a></p></div>';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });
  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
}
