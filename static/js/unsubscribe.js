$("#unsubscribe").on("submit", function (e) {
    e.preventDefault()
    var form = $(this);
    $.ajax({
        url: '/unsubscribe/',
        data: form.serialize(),
        type: 'post',
        dataType: 'json',
        success: function (data) {
            if (data.status) {
            $(".application_create_form").html(data.html_form)
              toastr.success('Вы успешно были отписаны от рассылки!',
                  "Отписка от рассылки",
                  {
                    "closeButton": true,
                    "progressBar": false,
                    "positionClass": "toast-top-center",
                  },
                  )// <-- This is just a placeholder for now for testing
            } else {
                toastr.error('Вы не были ещё подписаны на рассылку или убедитесь в достоверности данных',
                  "Ошибка",
                  {
                    "closeButton": true,
                    "progressBar": false,
                    "positionClass": "toast-top-center",
                  },
                  )
            }
        },
    });
    return false;
    }
)