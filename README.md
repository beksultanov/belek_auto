# Epidemiological Platform

The service uses the following technologies:

- `Python 3.8`
- `Django 3.1.6`
- `Huey (task queue)`
- `Gunicorn`
- `NGINX 1.19`
- `PostgreSQL 12.4`
- `Redis 6.0.9`
- `Docker`

## How to run production environment via docker

Use `docker-compose up --build -d` for pull images and start

---

### Environment variables

| Key    | Description   |    Default value  |
| :---         |     :---      |          :--- |
| `DEBUG`  | DEBUG  | True |
| `SECRET_KEY`  | SECRET_KEY  | gt6=4#ma!8av=_4g+!#q-28^udn4)=$pcckmu-1dgvi(dizf5k |
| `POSTGRES_USER`  | Postgres username |   belek-auto   |
| `POSTGRES_PASSWORD`  | Postgres password |  belek-auto    |
| `POSTGRES_DB`  | Postgres database name | belek-auto |
| `POSTGRES_HOST`  | Postgres host | 127.0.0.1 |
| `POSTGRES_PORT`  | Postgres port | 5432 |
| `REDIS_URL`  | Redis url | redis://127.0.0.1:6379/0 |
