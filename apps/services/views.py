from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.template.loader import render_to_string

from django.views.generic import TemplateView, FormView, ListView, DetailView
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist


from .forms import SubscriberForm, ApplicationForm, UnsubscribeForm
from .models import (
    MainMenuQuantity,
    PartnerLogos,
    OurAchievement,
    CustomerReview,
    ContactDetails,
    Post,
    Subscriber
)

from .tasks import send_email_new_application


def get_index_page(request):
    if request.method == "GET":
        quantity = MainMenuQuantity.objects.all().first()
        partner_logos = PartnerLogos.objects.all()
        achievements = OurAchievement.objects.all()[:3]
        latest_posts = Post.objects.all().order_by("-created")[:3]
        reviews = CustomerReview.objects.filter(active=True)
        form = SubscriberForm()
        context = {
            "quantity": quantity,
            "partner_logos": partner_logos,
            "achievements": achievements,
            "latest_posts": latest_posts,
            "reviews": reviews,
            "form": form,
        }
        return render(request, "index.html", context=context)
    elif request.method == "POST":
        form = SubscriberForm(request.POST)
        if form.is_valid():
            Subscriber.objects.update_or_create(email=form.cleaned_data.get("email"))
            data = {
                "status": True
            }
            return JsonResponse(data)


class ContactFormView(TemplateView):
    template_name = "contact.html"

    def get_context_data(self, **kwargs):
        context = super(ContactFormView, self).get_context_data(**kwargs)
        contact = ContactDetails.objects.all().first()
        context.update({
            "contact": contact,
        })
        return context


class AboutView(TemplateView):
    template_name = "about-us.html"

    def get_context_data(self, **kwargs):
        context = super(AboutView, self).get_context_data(**kwargs)
        try:
            contact = ContactDetails.objects.all().first()
        except ContactDetails.DoestNotExist:
            contact = None
        context.update({"contact": contact})
        return context


class PostListView(ListView):
    model = Post
    template_name = "post_list.html"
    paginate_by = 3
    queryset = Post.objects.filter(active=True).order_by("-created")


class PostDetailView(DetailView):
    model = Post
    template_name = "post_detail.html"


def create_application(request):
    data = dict()
    if request.method == 'POST':
        form = ApplicationForm(request.POST)
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            data['html_form'] = render_to_string(
                template_name='partial_application_form.html',
                context={
                    'application_form': ApplicationForm()
                },
                request=request
            )
            send_email_new_application(
                full_name=form.cleaned_data.get("full_name"),
                phone_number=form.cleaned_data.get("phone_number"),
                amount=form.cleaned_data.get("amount"),
                email=form.cleaned_data.get("email")
            )
        else:
            data['form_is_valid'] = False
    return JsonResponse(data)


def unsubscribe(request):
    data = dict()
    if request.method == 'POST':
        form = UnsubscribeForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('email')
            try:
                Subscriber.objects.get(email=email).delete()
                data['status'] = True
            except ObjectDoesNotExist:
                data['status'] = False
        return JsonResponse(data)
    elif request.method == 'GET':
        context = {
            "form": UnsubscribeForm()
        }
        return render(request, "unsubscribe.html", context=context)

