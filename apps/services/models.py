from django.contrib.auth import get_user_model
from django.db import models
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.translation import gettext as _
from django.db.models.signals import post_save

# Create your models here.
User = get_user_model()
# На главное меню цифры

from .utils import upload_instance_file


class MainMenuQuantity(models.Model):
    satisfied_customers = models.IntegerField(
        verbose_name=_("Довольные клиенты"))
    purchased_cars = models.IntegerField(verbose_name=_("Приобретено Машин"))
    waiting_for_dream = models.IntegerField(verbose_name=_("В ожидании мечты"))

    class Meta:
        verbose_name = _("Число на главном меню")
        verbose_name_plural = _("Числа на главном меню")

    def __str__(self):
        return f"{self.satisfied_customers}"


class PartnerLogos(models.Model):
    logo = models.ImageField(
        verbose_name=_("Лого партнеров"),
        upload_to=upload_instance_file
    )
    name = models.CharField(
        verbose_name=_("Название компании или партнера"),
        max_length=255, null=True
    )
    partner_url = models.URLField(
        verbose_name=_("Ccылка на сайт партнера"),
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = _("Лого партнера")
        verbose_name_plural = _("Лого партнеров")
        ordering = ['name']

    def __str__(self):
        return f"{self.name}"


class OurAchievement(models.Model):
    image = models.ImageField(
        verbose_name=_("Картинка"),
        upload_to=upload_instance_file
    )
    text = models.TextField(verbose_name=_("Текст"))
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _("Наше достижение")
        verbose_name_plural = _("Наши достижения")
        ordering = ['-created']

    def __str__(self):
        return f"{self.created}"


class CustomerReview(models.Model):
    image = models.ImageField(
        verbose_name=_("Фото клиента"),
        upload_to=upload_instance_file
    )
    name = models.CharField(verbose_name=_("Имя клиента"), max_length=150)
    text = models.TextField(verbose_name=_("Отзыв клиента"))
    active = models.BooleanField(verbose_name=_("Активная"), default=True)

    class Meta:
        verbose_name = _("Отзыв клиента")
        verbose_name_plural = _("Отзывы клиентов")
        ordering = ['name', ]

    def __str__(self):
        return f"{self.name}"


class Subscriber(models.Model):
    email = models.EmailField(verbose_name=_("Электронная почта"))
    active = models.BooleanField(verbose_name=_("Активная"), default=True)

    class Meta:
        verbose_name = _("Подписчик на рассылки")
        verbose_name_plural = _("Подписчики на рассылки")
        ordering = ['email']

    def __str__(self):
        return f"{self.email}"


class Post(models.Model):
    user = models.ForeignKey(
        User,
        verbose_name=_("Автор"),
        on_delete=models.SET_NULL,
        null=True
    )
    title = models.CharField(verbose_name=_("Название поста"), max_length=255)
    title_kg = models.CharField(
        verbose_name=_("Название поста на кыргызском"),
        max_length=255,
        default=''
    )
    text = models.TextField(verbose_name=_("Текст"))
    text_kg = models.TextField(verbose_name=_("Текст на кыргызском"), default='')
    created = models.DateTimeField(auto_now_add=True, db_index=True)
    updated = models.DateTimeField(auto_now=True)
    video_link = models.URLField(
        verbose_name=_("Ссылка на видео"),
        max_length=255,
        null=True,
        blank=True
    )
    image = models.ImageField(
        verbose_name=_("Фото"),
        upload_to=upload_instance_file,
        null=True,
        blank=True
    )
    active = models.BooleanField(verbose_name=_("Активная"), default=True)

    class Meta:
        verbose_name = _("Пост")
        verbose_name_plural = _("Посты")
        ordering = ['-created']

    class TranslatableMeta:
        fields = ('title', 'text')

    def __str__(self):
        return f"{self.title}"

    def get_absolute_url(self):
        return reverse('post_detail', args=[self.pk])


class ContactDetails(models.Model):
    phone1 = models.CharField(max_length=100, verbose_name=_("Номер телефона 1"))
    phone2 = models.CharField(max_length=100, verbose_name=_("Номер телефона 2"))
    address = models.CharField(max_length=255, verbose_name=_("Адрес"))
    address_kg = models.CharField(max_length=255, verbose_name="Адрес на кыргызском", default='')
    email = models.EmailField(verbose_name=_("Электронная почта"))

    class Meta:
        verbose_name = _("Контактные данные")
        verbose_name_plural = _("Контактные данные")

    def __str__(self):
        return f"{self.email}"


class Application(models.Model):
    full_name = models.CharField(max_length=100, verbose_name=_("Ф.И.О."))
    email = models.EmailField(verbose_name=_("Электроная почта"), blank=True)
    amount = models.DecimalField(verbose_name=_("Примерная сумма"), decimal_places=0, max_digits=20)
    phone_number = models.CharField(max_length=12, verbose_name=_("Номер телефона"))
    created = models.DateTimeField(auto_now_add=True, verbose_name=_("Дата отправки заявления"))
    updated = models.DateTimeField(auto_now=True, verbose_name=_("Обновление заявки"))

    class Meta:
        verbose_name = _("Заявление")
        verbose_name_plural = _("Заявления")
        ordering = ['-created']

    def __str__(self):
        return f"{self.full_name}"


from django.db.models.signals import post_save
from .tasks import send_new_post

post_save.connect(send_new_post, sender=Post)
