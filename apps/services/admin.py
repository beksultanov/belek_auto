from django.contrib import admin
from . import models
# Register your models here.


@admin.register(models.MainMenuQuantity)
class MainMenuQuantityAdmin(admin.ModelAdmin):
    list_display = ['satisfied_customers', 'purchased_cars', 'waiting_for_dream']


@admin.register(models.PartnerLogos)
class PartnerLogosAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']


@admin.register(models.OurAchievement)
class OurAchievementAdmin(admin.ModelAdmin):
    list_display = ['created', 'text']

@admin.register(models.CustomerReview)
class CustomerReviewAdmin(admin.ModelAdmin):
    list_display = ['name', 'active']
    search_fields = ['name']
    list_filter = ['active']


@admin.register(models.Subscriber)
class SubscriberAdmin(admin.ModelAdmin):
    list_display = ['email', 'active']
    search_fields = ['email']
    list_filter = ['active']


@admin.register(models.Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'user', 'created']
    search_fields = ['title', 'title_kg', 'user']
    list_filter = ['user', 'active', 'created']


@admin.register(models.ContactDetails)
class ContactDetailsAdmin(admin.ModelAdmin):
    list_display = ['phone1', 'phone2', 'email', 'address']


@admin.register(models.Application)
class ApplicationAdmin(admin.ModelAdmin):
    list_display = ['full_name', 'email', 'amount', 'phone_number']
    search_fields = ['full_name', 'email', 'phone_number']
    list_filter = ['created']
    readonly_fields = ['full_name', 'email', 'phone_number', 'amount']
