from . import views
from django.urls import path, include

urlpatterns = [
    path("", views.get_index_page, name='index_page'),
    path("contacts/", views.ContactFormView.as_view(), name='contacts'),
    path("create_application/", views.create_application, name="create_application"),
    path("about_us/", views.AboutView.as_view(), name="about_us"),
    path("post_list/", views.PostListView.as_view(), name='post_list'),
    path("post_detail/<int:pk>/", views.PostDetailView.as_view(), name="post_detail"),
    path("unsubscribe/", views.unsubscribe, name='unsubscribe'),
]
