from django.conf import settings
from django.core.mail import send_mail, send_mass_mail
from django.utils.translation import gettext as _
from huey.contrib.djhuey import task

from .models import Subscriber

domain = settings.BASE_URL


def get_email_list():
    subscribers_list = []
    subscribers = Subscriber.objects.filter(active=True)
    for subscriber in subscribers:
        subscribers_list.append(subscriber.email)
    return subscribers_list


@task()
def send_email_new_application(full_name, phone_number, amount, email):
    message = f"""
            Вы получили новую заявку от {full_name}.
            Номер телефона:{phone_number},  
            Почта отправителя: {email},
            Примерная сумма:{amount}. 
        """
    send_mail(
        subject="Новая заявка",
        message=message,
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[settings.EMAIL_HOST_USER],
    )


@task()
def send_new_post(sender, instance, **kwargs):
    message = f"""
            Здравствуйте! На сайте belekavto.kg был опубликован новый пост.
            Тема поста: {instance.title}
            Что бы узнать подробнее перейдите по ссылке ниже: 
            {domain}{instance.get_absolute_url()} \n
            Если вы желаете отписаться от рассылки, то можете перейти по ссылке ниже:
            {domain}/unsubscribe/
              
            С уважением, команда "Белек Авто".
    """
    recipients_list = get_email_list()
    send_mail(
        subject=f"Новый пост! {instance.title}",
        message=message,
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=recipients_list,
    )
