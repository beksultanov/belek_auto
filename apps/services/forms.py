from django import forms

from .models import Subscriber, Application
from django.utils.translation import ugettext_lazy as _


class SubscriberForm(forms.ModelForm):

    class Meta:
        model = Subscriber
        fields = ['email']
        widgets = {
            "email": forms.EmailInput(attrs={"placeholder": "example@example.com", "type": "email"})
        }
        labels = {
            "email": ""
        }


class UnsubscribeForm(forms.ModelForm):
    class Meta:
        model = Subscriber
        fields = ['email']
        widgets = {
            "email": forms.EmailInput(attrs={"placeholder": "example@example.com", "type": "email"})
        }
        labels = {
            "email": ""
        }


class ApplicationForm(forms.ModelForm):

    class Meta:
        model = Application
        fields = [
            'full_name',
            'email',
            'amount',
            'phone_number',
        ]
        widgets = {
            "full_name": forms.TextInput(attrs={
                'placeholder': _("Ф.И.О."),
                "class": "contact-form-input"
            }),
            "email": forms.EmailInput(attrs={
                'placeholder': _("Ваша электронная почта"),
                "class": "contact-form-input",
                "id": "application_email_id"
            }),
            "phone_number": forms.TextInput(attrs={
                'placeholder': _("Номер телефона(Пример: 0999331122)"),
                "class": "contact-form-input",
                "pattern": "[0-9]{10}"
            }),
            "amount": forms.NumberInput(attrs={
                'placeholder': _("Примерная сумма"),
                "class": "contact-form-input"
            }),
        }
        labels = {
            "full_name": "",
            "email": "",
            "amount": "",
            "phone_number": "",
        }



