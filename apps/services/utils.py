import uuid


def upload_instance_file(instance, filename):
    ext = filename.split(".")[-1]
    filename = f"{uuid.uuid4()}.{ext}"
    return "uploads/{}/{}".format(instance.__class__.__name__, filename)



