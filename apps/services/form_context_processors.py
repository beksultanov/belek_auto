from .forms import ApplicationForm


def application_form(request):
    return {
        "application_form": ApplicationForm()
    }
